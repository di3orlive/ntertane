'use strict';
var app = angular.module('app', [
    'ngAnimate',
    'ui.router',
    'ngRetina',
    'sun.scrollable',
    'angular-carousel'
]);

app.run(function($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from) {
        $rootScope.previousState = from.name;
        $rootScope.currentState = to.name;
    });
});

app.config([
    '$urlRouterProvider',
    '$stateProvider',
    'ngRetinaProvider',
    function(
    $urlRouterProvider,
    $stateProvider,
    ngRetinaProvider
    ){


    ngRetinaProvider.setInfix('_2x');

    $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('land', {
            url: "/",
            views: {"front": { templateUrl: 'views/front/land.html' }}
        })
        .state('login', {
            url: "/login",
            views: {"front": { templateUrl: 'views/front/login.html' }}
        })
        .state('signUp', {
            url: "/signUp",
            views: {"front": { templateUrl: 'views/front/signUp.html' }}
        })
        .state('in', {
            url: "/in",
            views: {"front": { templateUrl: 'views/back/in.html' }}
        })
        .state('in.home', {
            url: "/home",
            views: {"back": { templateUrl: 'views/back/in-home.html' }}
        })
        .state('in.music', {
            url: "/music",
            views: {"back": { templateUrl: 'views/back/in-music.html' }}
        })
        .state('in.albums', {
            url: "/albums",
            views: {"back": { templateUrl: 'views/back/in-albums.html' }}
        })
        .state('in.albumDetails', {
            url: "/album/details/:detailId",
            views: {"back": { templateUrl: 'views/back/in-albums-details.html' }}
        })
        .state('in.artists', {
            url: "/artists",
            views: {"back": { templateUrl: 'views/back/in-artists.html' }}
        })
        .state('in.artistDetails', {
            url: "/artists/details/:artistId",
            views: {"back": { templateUrl: 'views/back/in-artists-derails.html' }}
        })
        .state('in.songs', {
            url: "/songs",
            views: {"back": { templateUrl: 'views/back/in-songs.html' }}
        })
        .state('in.playlist', {
            url: "/playlist",
            views: {"back": { templateUrl: 'views/back/in-playlist.html' }}
        })
        .state('in.movie', {
            url: "/movie",
            views: {"back": { templateUrl: 'views/back/in-movie.html' }}
        })
        .state('in.movieDetails', {
            url: "/movie/details/:movieId",
            views: {"back": { templateUrl: 'views/back/in-movie-details.html' }}
        })
        .state('in.radio', {
            url: "/radio",
            views: {"back": { templateUrl: 'views/back/in-radio.html' }}
        });
}]);

app.controller('mainCtrl', ['$scope', function ($scope) {

}]);

app.controller('inCtrl', ['$scope', function ($scope) {
    $scope.testArray = [
        {id:1,title:'lorem'},
        {id:2,title:'lorem'},
        {id:3,title:'lorem'},
        {id:4,title:'lorem'},
        {id:5,title:'lorem'},
        {id:6,title:'lorem'},
        {id:7,title:'lorem'},
        {id:8,title:'lorem'},
        {id:9,title:'lorem'},
        {id:10,title:'lorem'},
        {id:11,title:'lorem'},
        {id:12,title:'lorem'},
        {id:13,title:'lorem'},
        {id:14,title:'lorem'},
        {id:15,title:'lorem'},
        {id:16,title:'lorem'},
        {id:17,title:'lorem'},
        {id:18,title:'lorem'},
        {id:19,title:'lorem'},
        {id:20,title:'lorem'},
        {id:21,title:'lorem'},
        {id:22,title:'lorem'},
        {id:23,title:'lorem'},
        {id:24,title:'lorem'}
    ];

    /**vars inside**/
    $scope.var = {
        showMenu: false
    };

    /**tabs inside**/
    $scope.tab = 1;

    $scope.setTab = function (newTab) {
        $scope.tab = newTab;
    };
    $scope.isTab = function (tab) {
        return $scope.tab === tab;
    };
}]);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

app.directive("scrollToAnchor", function () {
    return {
        restrict: "A",
        scope: {
            scrollToAnchor: '@'
        },
        link: function (scope, element, attr, form) {
            element.on("click", function () {
                $('html, body').animate({
                    scrollTop: $('#' + scope.scrollToAnchor).offset().top + 80
                }, 500);
                return false;
            });
        }
    };
});

app.directive("openThis", function () {
    return {
        restrict: "A",
        link: function (scope, element, attr, form) {
            element.on("click", function () {
                $('.m-btn-option').not($(element).parent()).removeClass('active');
                $(element).parent().toggleClass('active')
            });
        }
    };
});

app.directive("slideBox", function () {
    return {
        restrict: "A",
        scope: {
            slideBox: '=',
            showItems: '@'
        },
        link: function (scope, element, attr, form) {
            element.on("click", function (e) {
                if(e.target.getAttribute('data-ctrl') == 'prev') {
                    for(var i = 0; i < scope.showItems; i++){
                        scope.slideBox.unshift(scope.slideBox.pop(0));
                    }
                    scope.$apply();
                }else if(e.target.getAttribute('data-ctrl') == 'next'){
                    for(i = 0; i < scope.showItems; i++){
                        scope.slideBox.push(scope.slideBox.shift(0));
                    }
                    scope.$apply();
                }
            });
        }
    };
});

//app.directive("loader", function ($rootScope, $timeout) {
//    return {
//        restrict: "E",
//        link: function (scope, element, attr, form) {
//            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options){
//                $(element).show();
//                $timeout(function(){
//                    $(element).hide();
//                }, 1000)
//            });
//            $timeout(function(){
//                $(element).hide();
//            }, 1000)
//        }
//    };
//});
//


























